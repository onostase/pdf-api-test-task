﻿using Microsoft.EntityFrameworkCore;
using PDFStorage.Core.Contracts.QueryModels;
using PDFStorage.Core.Contracts.Repositories;
using PDFStorage.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PDFStorage.Persistence.Repositories
{
    public class FileRepository : IFileRepository
    {
        private readonly ApplicationDbContext _context;

        public FileRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<bool> AddAsync(FileEntity entity, CancellationToken cancellationToken = default)
        {
            _context.Files.Add(entity);
            return await SaveAsync(cancellationToken).ConfigureAwait(false);
        }

        public async Task<bool> ExistsAsync(int id, CancellationToken cancellationToken = default)
        {
            return await _context.Files.AnyAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<FileEntity>> GetAllAsync(IFileQueryModel queryModel, CancellationToken cancellationToken = default)
        {
            var query = _context.Files.AsQueryable();
            query = ApplyQuery(query, queryModel);
            query = ApplySort(query, queryModel.OrderBy);
            return await query.ToListAsync(cancellationToken).ConfigureAwait(false);
        }

        public async Task<FileEntity> GetAsync(int id, CancellationToken cancellationToken = default)
        {
            return await _context.Files.FindAsync(id);
        }

        public async Task<bool> RemoveAsync(FileEntity entity, CancellationToken cancellationToken = default)
        {
            _context.Files.Remove(entity);
            return await SaveAsync(cancellationToken).ConfigureAwait(false);
        }

        public async Task<bool> SaveAsync(CancellationToken cancellationToken = default)
        {
            var result = await _context.SaveChangesAsync().ConfigureAwait(false);
            return result >= 0;
        }

        private IQueryable<FileEntity> ApplyDefaultSort(IQueryable<FileEntity> query)
        {
            return query.OrderBy(x => x.Name);
        }

        private IQueryable<FileEntity> ApplySort(IQueryable<FileEntity> query, string orderByQueryString)
        {
            if (string.IsNullOrWhiteSpace(orderByQueryString))
            {
                return ApplyDefaultSort(query);
            }

            var sortParam = GetValidSortingParameterOrThrow(orderByQueryString);
            if (sortParam.sortBy == null)
            {
                return ApplyDefaultSort(query);
            }
            var sortWithOrder = sortParam.sortOrder.EndsWith("desc") ? $"{sortParam.sortBy}_desc" : sortParam.sortBy;
            switch (sortWithOrder)
            {
                case "id":
                    {
                        query = query.OrderBy(x => x.Id);
                        break;
                    }
                case "id_desc":
                    {
                        query = query.OrderByDescending(x => x.Id);
                        break;
                    }
                case "name_desc":
                    {
                        query = query.OrderByDescending(x => x.Name);
                        break;
                    }
                case "name":
                default:
                    {
                        query = query.OrderBy(x => x.Name);
                        break;
                    }
            }

            return query;
        }

        private (string sortBy, string sortOrder) GetValidSortingParameterOrThrow(string orderByQueryString)
        {
            var orderParams = orderByQueryString.ToLower().Trim().Split(',');
            //Only one parameter sorting supported
            if (orderParams.Length > 1)
            {
                throw new ArgumentException("More than one sorting attribute provided, but only one is supported.", nameof(orderByQueryString));
            } 
            var param = orderParams.First();

            if (string.IsNullOrWhiteSpace(param))
            {
                return (null, null);
            }

            var splitParams = param.Split(' ');
            var propertyInfos = typeof(FileEntity).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            var objectProperty = propertyInfos.FirstOrDefault(pi => pi.Name.Equals(splitParams[0], StringComparison.InvariantCultureIgnoreCase));

            if (objectProperty == null)
            {
                throw new ArgumentException($"Sorting by unsupported field: {splitParams[0]}", nameof(orderByQueryString));
            }
            return (splitParams[0], splitParams.Length > 1 ? splitParams[1] : "asc");
        }

        protected virtual IQueryable<FileEntity> ApplyQuery(IQueryable<FileEntity> query, IFileQueryModel queryModel)
        {
            if (queryModel.Ids != null && queryModel.Ids.Any())
            {
                query = query.Where(x => queryModel.Ids.Contains(x.Id));
            }
            if (queryModel.Names != null && queryModel.Names.Any())
            {
                var lowerNames = queryModel.Names.Select(i => i.ToLower()).ToList();
                query = query.Where(x => lowerNames.Contains(x.Name.ToLower()));
            }
            if (queryModel.Skip.HasValue && queryModel.Skip.Value > 0)
            {
                query = query.Skip(queryModel.Skip.Value);
            }
            if (queryModel.Take.HasValue && queryModel.Take.Value > 0)
            {
                query = query.Take(queryModel.Take.Value);
            }
            return query;
        }

    }
}
