﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using PDFStorage.Core.Entities;

namespace PDFStorage.Persistence.Configurations
{
    public class FileEntityConfiguration : IEntityTypeConfiguration<FileEntity>
    {
        public void Configure(EntityTypeBuilder<FileEntity> builder)
        {
            builder.ToTable("File");
            builder.HasKey(entity => entity.Id);
            builder.Property(x => x.Id).UseHiLo();
            builder.Property(entity => entity.Name).HasMaxLength(500).IsRequired();
            builder.Property(entity => entity.LocalFilePath).HasMaxLength(1500).IsRequired();
        }
    }
}
