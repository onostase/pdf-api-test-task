﻿using Microsoft.EntityFrameworkCore;
using PDFStorage.Core.Contracts;
using PDFStorage.Core.Entities;
using PDFStorage.Persistence.Configurations;
using System;

namespace PDFStorage.Persistence
{
    public class ApplicationDbContext : DbContext, IApplicationDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<FileEntity> Files { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new FileEntityConfiguration());
        }
    }
}
