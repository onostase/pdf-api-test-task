﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using PDFStorage.Core.Contracts;
using PDFStorage.Core.Contracts.Repositories;
using PDFStorage.Persistence.Repositories;

namespace PDFStorage.Persistence.IoC
{
    public static class PersistenceRegistrations
    {
        public static IServiceCollection AddPersistence(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(connectionString), ServiceLifetime.Transient);

            services.AddScoped<IApplicationDbContext>(provider => provider.GetService<ApplicationDbContext>());
            services.AddTransient<IFileRepository, FileRepository>();
            return services;
        }
    }
}
