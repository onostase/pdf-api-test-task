﻿using System.IO;

namespace PDFStorage.Core.Models
{
    public class FileDownloadInfo
    {
        public string ContentType { get; set; }
        public Stream FileStream { get; set; }
        public string FileName { get; set; }
    }
}
