﻿namespace PDFStorage.Core.Models
{
    public class FileViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
