﻿using Microsoft.Extensions.DependencyInjection;
using PDFStorage.Core.Contracts.Services;
using PDFStorage.Core.Contracts.Settings;
using PDFStorage.Core.Services;
using PDFStorage.Core.Settings;

namespace PDFStorage.Core.IoC
{
    public static class CoreRegistrations
    {
        public static IServiceCollection AddCore(this IServiceCollection services)
        {
            services.AddScoped<IUploadSettings, UploadSettings>();
            services.AddTransient<IManagementService, ManagementService>();
            services.AddTransient<IUploadService, UploadService>();
            services.AddTransient<IDownloadService, DownloadService>();
            return services;
        }
    }
}
