﻿using Microsoft.Extensions.Configuration;
using PDFStorage.Core.Contracts.Settings;

namespace PDFStorage.Core.Settings
{
    public class UploadSettings : IUploadSettings
    {
        public UploadSettings(IConfiguration configuration)
        {
            RootFolderPath = configuration["Upload:RootFolder"];
        }

        public string RootFolderPath { get; }
    }
}
