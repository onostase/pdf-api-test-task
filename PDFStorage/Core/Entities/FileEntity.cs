﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PDFStorage.Core.Entities
{
    public class FileEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LocalFilePath { get; set; }
    }
}
