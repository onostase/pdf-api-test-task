﻿using PDFStorage.Core.Contracts.QueryModels;
using PDFStorage.Core.Entities;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PDFStorage.Core.Contracts.Repositories
{
    public interface IFileRepository
    {
        Task<IEnumerable<FileEntity>> GetAllAsync(IFileQueryModel queryModel, CancellationToken cancellationToken = default);
        Task<FileEntity> GetAsync(int id, CancellationToken cancellationToken = default);
        Task<bool> ExistsAsync(int id, CancellationToken cancellationToken = default);
        Task<bool> AddAsync(FileEntity entity, CancellationToken cancellationToken = default);
        Task<bool> SaveAsync(CancellationToken cancellationToken = default);
        Task<bool> RemoveAsync(FileEntity entity, CancellationToken cancellationToken = default);
    }
}
