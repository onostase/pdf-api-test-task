﻿using System.Collections.Generic;

namespace PDFStorage.Core.Contracts.QueryModels
{
    public interface IFileQueryModel
    {
        IEnumerable<int> Ids { get; }
        IEnumerable<string> Names { get; }
        int? Take { get; }
        int? Skip { get; }
        string OrderBy { get; }
    }
}
