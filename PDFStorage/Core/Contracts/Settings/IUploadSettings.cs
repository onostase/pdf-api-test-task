﻿namespace PDFStorage.Core.Contracts.Settings
{
    public interface IUploadSettings
    {
        string RootFolderPath { get; }
    }
}
