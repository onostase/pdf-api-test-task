﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace PDFStorage.Core.Contracts.Services
{
    public interface IUploadService
    {
        Task<string> DownloadFileToLocalFolderAsync(Stream fileStream, string fileName);
    }
}
