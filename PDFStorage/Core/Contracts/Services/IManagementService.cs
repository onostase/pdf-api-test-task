﻿using PDFStorage.Core.Contracts.QueryModels;
using PDFStorage.Core.Models;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace PDFStorage.Core.Contracts.Services
{
    public interface IManagementService
    {
        Task<IEnumerable<FileViewModel>> GetAllAsync(IFileQueryModel queryModel, CancellationToken cancellationToken = default);
        Task<FileViewModel> GetAsync(int id, CancellationToken cancellationToken = default);
        Task<bool> DeleteAsync(int fileId, CancellationToken cancellationToken = default);
        Task<FileViewModel> CreateFileRecordAsync(string fileName, Stream fileStream, CancellationToken cancellationToken = default);
    }
}
