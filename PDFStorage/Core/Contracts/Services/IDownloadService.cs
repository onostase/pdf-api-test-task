﻿using PDFStorage.Core.Models;
using System.Threading;
using System.Threading.Tasks;

namespace PDFStorage.Core.Contracts.Services
{
    public interface IDownloadService
    {
        Task<FileDownloadInfo> GetFileDownloadAsync(int id, CancellationToken cancellationToken = default);
    }
}
