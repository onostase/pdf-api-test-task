﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PDFStorage.Core.Exceptions
{
    [Serializable]
    public class FileProcessingFailedException : Exception
    {
        public FileProcessingFailedException(string message) : base(message)
        { }
    }
}
