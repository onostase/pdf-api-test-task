﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PDFStorage.Core.Exceptions
{
    [Serializable]
    public class RecordNotFoundException : Exception
    {
        public RecordNotFoundException(int id) : base($"Record with id {id} not found")
        { }
    }
}
