﻿using PDFStorage.Core.Contracts.Repositories;
using PDFStorage.Core.Contracts.Services;
using PDFStorage.Core.Exceptions;
using PDFStorage.Core.Models;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace PDFStorage.Core.Services
{
    public class DownloadService : IDownloadService
    {
        private readonly IFileRepository _repository;

        public DownloadService(IFileRepository repository)
        {
            _repository = repository;
        }

        public async Task<FileDownloadInfo> GetFileDownloadAsync(int id, CancellationToken cancellationToken = default)
        {
            var entity = await _repository.GetAsync(id, cancellationToken).ConfigureAwait(false);
            if (entity == null)
            {
                throw new RecordNotFoundException(id);
            }
            var stream = new FileStream(entity.LocalFilePath, FileMode.Open, FileAccess.Read, FileShare.None);
            return new FileDownloadInfo { FileName = entity.Name, FileStream = stream, ContentType = "application/pdf" };
        }
    }
}
