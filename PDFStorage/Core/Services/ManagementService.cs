﻿using Microsoft.Extensions.Logging;
using PDFStorage.Core.Contracts.QueryModels;
using PDFStorage.Core.Contracts.Repositories;
using PDFStorage.Core.Contracts.Services;
using PDFStorage.Core.Entities;
using PDFStorage.Core.Exceptions;
using PDFStorage.Core.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PDFStorage.Core.Services
{
    public class ManagementService : IManagementService
    {
        private readonly IUploadService _uploadService;
        private readonly IFileRepository _repository;
        private readonly ILogger<ManagementService> _logger;

        public ManagementService(IUploadService uploadService, IFileRepository repository, ILogger<ManagementService> logger)
        {
            _uploadService = uploadService;
            _repository = repository;
            _logger = logger;
        }

        public async Task<FileViewModel> CreateFileRecordAsync(string fileName, Stream fileStream, CancellationToken cancellationToken = default)
        {
            _logger.LogInformation($"Received file {fileName} for processing.");
            var fullName = await _uploadService.DownloadFileToLocalFolderAsync(fileStream, fileName).ConfigureAwait(false);
            if (string.IsNullOrEmpty(fullName))
            {
                throw new FileProcessingFailedException($"Error downloading file {fileName}.");
            }
            _logger.LogInformation($"File {fileName} successfully downloaded, creating database record...");
            var model = new FileEntity { Name = fileName, LocalFilePath = fullName };
            await _repository.AddAsync(model, cancellationToken).ConfigureAwait(false);
            _logger.LogInformation($"Database file record with id {model.Id} created.");
            return new FileViewModel { Id = model.Id, Name = model.Name };
        }

        public async Task<bool> DeleteAsync(int fileId, CancellationToken cancellationToken = default)
        {
            var entity = await _repository.GetAsync(fileId, cancellationToken).ConfigureAwait(false);
            if (entity == null)
            {
                throw new RecordNotFoundException(fileId);
            }
            if (_logger.IsEnabled(LogLevel.Debug))
            {
                _logger.LogDebug($"Deleting file with id {fileId}");
            }
            return await _repository.RemoveAsync(entity, cancellationToken).ConfigureAwait(false);
        }

        public async Task<FileViewModel> GetAsync(int id, CancellationToken cancellationToken = default)
        {
            var entity = await _repository.GetAsync(id, cancellationToken).ConfigureAwait(false);
            if (entity == null)
            {
                throw new RecordNotFoundException(id);
            }
            return ConvertEntity(entity);
        }

        public async Task<IEnumerable<FileViewModel>> GetAllAsync(IFileQueryModel queryModel, CancellationToken cancellationToken = default)
        {
            var entities = await _repository.GetAllAsync(queryModel, cancellationToken).ConfigureAwait(false);
            return entities.Select(ConvertEntity).ToList();
        }

        private FileViewModel ConvertEntity(FileEntity entity)
        {
            return new FileViewModel { Id = entity.Id, Name = entity.Name };
        }
    }
}
