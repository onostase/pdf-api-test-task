﻿using PDFStorage.Core.Contracts.Services;
using PDFStorage.Core.Contracts.Settings;
using System;
using System.IO;
using System.Threading.Tasks;

namespace PDFStorage.Core.Services
{
    public class UploadService : IUploadService
    {
        private readonly IUploadSettings _settings;

        public UploadService(IUploadSettings settings)
        {
            _settings = settings;
        }

        public async Task<string> DownloadFileToLocalFolderAsync(Stream fileStream, string fileName)
        {
            var folder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, _settings.RootFolderPath);
            if (!Directory.Exists(folder))
            {
                Directory.CreateDirectory(folder);
            }
            var fullName = Path.Combine(folder, $"{fileName}_{DateTime.Now.ToString("yyyyMdd_HHms")}");
            using (var stream = new FileStream(fullName, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                await fileStream.CopyToAsync(stream).ConfigureAwait(false);
            }
            return fullName;
        }
    }
}
