﻿using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Web;
using PDFStorage.API.Validators;
using PDFStorage.Core.IoC;
using PDFStorage.Persistence.IoC;

namespace PDFStorage.API.IoC
{
    public static class ApiRegistrations
    {
        public static IServiceCollection RegisterAPIServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IValidator<IFormFile>, UploadFileValidator>();
            services.AddNLog();
            services.AddPersistence(configuration.GetConnectionString("DefaultConnection"));
            services.AddCore();
            return services;
        }

        private static IServiceCollection AddNLog(this IServiceCollection services)
        {
            services.AddLogging(logBuilder =>
            {
                logBuilder.ClearProviders();
                logBuilder.AddNLog("NLog.config");
                logBuilder.SetMinimumLevel(LogLevel.Trace);
            });
            return services;
        }
    }
}
