﻿using PDFStorage.Core.Contracts.QueryModels;
using System.Collections.Generic;

namespace PDFStorage.API.Models
{
    public class FileQueryModel : IFileQueryModel
    {
        public IEnumerable<int> Ids { get; set; } = new List<int>();

        public IEnumerable<string> Names { get; set; } = new List<string>();

        public int? Take { get; set; }

        public int? Skip { get; set; }

        public string OrderBy { get; set; }
    }
}
