﻿namespace PDFStorage.API.Models
{
    public class FilePresentationModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
    }
}
