﻿using FluentValidation;
using Microsoft.AspNetCore.Http;

namespace PDFStorage.API.Validators
{
    public class UploadFileValidator : AbstractValidator<IFormFile>
    {
        private const int MaxLength = 5 * 1024 * 1024; // 5Mb
        public UploadFileValidator()
        {
            RuleFor(x => x.Length).NotNull().LessThanOrEqualTo(MaxLength)
                .WithMessage("File size is larger than allowed");

            RuleFor(x => x.ContentType).NotNull().Must(x => x.Equals("application/pdf"))
                .WithMessage("File type is not supported");
        }
    }
}
