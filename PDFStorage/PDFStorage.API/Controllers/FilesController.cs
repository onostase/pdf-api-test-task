﻿using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PDFStorage.API.Models;
using PDFStorage.Core.Contracts.Services;
using PDFStorage.Core.Exceptions;
using PDFStorage.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PDFStorage.API.Controllers
{
    /// <summary>
    /// Files CRD controller
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class FilesController : ControllerBase
    {
        private readonly IValidator<IFormFile> _validator;
        private readonly IManagementService _service;
        private readonly ILogger<DownloadController> _logger;

        public FilesController(IValidator<IFormFile> validator, IManagementService service, ILogger<DownloadController> logger)
        {
            _validator = validator;
            _service = service;
            _logger = logger;
        }

        /// <summary>
        /// Upload a file and create a file record based on it
        /// </summary>
        /// <response code="201">File record is created, returns record details</response>
        /// <response code="400">Invalid file provided, returns validation error</response>
        /// <response code="500">An error has occured during the file processing</response>
        [ProducesResponseType(typeof(FilePresentationModel), StatusCodes.Status201Created)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        [ProducesErrorResponseType(typeof(string))]        
        [Consumes("multipart/form-data")]
        [Produces("application/json", "text/plain")]
        [HttpPost]
        public async Task<IActionResult> PostAsync(IFormFile file, CancellationToken cancellationToken)
        {
            if (_logger.IsEnabled(LogLevel.Trace))
            {
                _logger.LogTrace($"New file upload request for {file.FileName}, validation started...");
            }
            var validationResult = _validator.Validate(file);
            if (!validationResult.IsValid)
            {
                if (_logger.IsEnabled(LogLevel.Trace))
                {
                    _logger.LogTrace($"{file.FileName} failed validation.");
                }
                return BadRequest(string.Join(", ", validationResult.Errors.Select(x => x.ErrorMessage)));
            }
            try
            {
                var record = await _service.CreateFileRecordAsync(file.FileName, file.OpenReadStream(), cancellationToken).ConfigureAwait(false);
                if (_logger.IsEnabled(LogLevel.Trace))
                {
                    _logger.LogTrace($"Created new record for {file.FileName} - {record.Id}.");
                }
                var url = Url.Link("GetRecord", new { controller = "files", id = record.Id });
                return Created(url, ConvertModel(record));
            }
            catch (FileProcessingFailedException ex)
            {
                _logger.LogError($"Upload request failed - {ex.Message}");
                return StatusCode(500, "Failed to process the file");
            }
        }

        /// <summary>
        /// Get all files records
        /// </summary>
        /// <param name="queryModel">Files search and sorting parameters. Sorting example: orderby=name asc</param>
        /// <response code="200">Returns records list</response>
        /// <response code="400">Invalid query provided, returns validation error</response>
        [ProducesResponseType(typeof(IEnumerable<FilePresentationModel>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        [Produces("application/json", "text/plain")]
        [HttpGet]
        public async Task<IActionResult> GetAllAsync([FromQuery]FileQueryModel queryModel, CancellationToken cancellationToken)
        {
            IEnumerable<FileViewModel> viewModels = null;
            try
            {
                viewModels = await _service.GetAllAsync(queryModel, cancellationToken).ConfigureAwait(false);
            }
            catch (ArgumentException ex)
            {
                _logger.LogError($"An error occured during Files List request: {ex.Message}");
                return BadRequest(ex.Message);
            }

            var result = viewModels.Select(ConvertModel).ToList();
            return Ok(result);
        }

        /// <summary>
        /// Get file record by id
        /// </summary>
        /// <response code="200">Returns record details</response>
        /// <response code="404">File with specified id is not found</response>
        [ProducesResponseType(typeof(FilePresentationModel), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        [Produces("application/json", "text/plain")]
        [HttpGet("{id}", Name = "GetRecord")]
        public async Task<IActionResult> GetAsync([FromRoute]int id, CancellationToken cancellationToken)
        {
            try
            {
                var viewModel = await _service.GetAsync(id, cancellationToken).ConfigureAwait(false);
                return Ok(ConvertModel(viewModel));
            }
            catch(RecordNotFoundException ex)
            {
                _logger.LogError($"Trying to delete unknown id {id}");
                return NotFound(ex.Message);
            }            
        }

        /// <summary>
        /// Delete file record by id
        /// </summary>
        /// <response code="200">Record has been successfully deleted</response>
        /// <response code="404">File with specified id is not found</response>
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status404NotFound)]
        [Produces("text/plain")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync([FromRoute]int id, CancellationToken cancellationToken)
        {
            try
            {
                await _service.DeleteAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (RecordNotFoundException ex)
            {
                _logger.LogError($"Attempt to delete unknown id {id}.");
                return NotFound(ex.Message);
            }
            return Ok("Successfully deleted");
        }


        private FilePresentationModel ConvertModel(FileViewModel viewModel)
        {
            return new FilePresentationModel
            {
                Id = viewModel.Id,
                Name = viewModel.Name,
                Location = Url.Link("Download", new { id = viewModel.Id, controller = "download" })
            };
        }
    }
}
