﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PDFStorage.Core.Contracts.Services;
using PDFStorage.Core.Exceptions;
using PDFStorage.Core.Models;
using System.Threading;
using System.Threading.Tasks;

namespace PDFStorage.API.Controllers
{
    /// <summary>
    /// Files download controller
    /// </summary>
    [ApiController]
    [Route("api/[controller]")]
    public class DownloadController : ControllerBase
    {
        private readonly IDownloadService _service;
        private readonly ILogger<DownloadController> _logger;

        public DownloadController(IDownloadService service, ILogger<DownloadController> logger)
        {
            _service = service;
            _logger = logger;
        }

        /// <summary>
        /// Get file for download by id
        /// </summary>
        /// <response code="200">Returns file</response>
        /// <response code="404">File with specified id is not found</response>
        [ProducesResponseType(typeof(FileStreamResult), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes. Status404NotFound)]
        [HttpGet("{id}", Name ="Download")]
        public async Task<IActionResult> DownloadAsync(int id, CancellationToken cancellationToken)
        {
            FileDownloadInfo fileInfo;
            try
            {
                fileInfo = await _service.GetFileDownloadAsync(id, cancellationToken).ConfigureAwait(false);
            }
            catch (RecordNotFoundException ex)
            {
                _logger.LogError($"Attempt to download non-existing file with id {id}.");
                return NotFound(ex.Message);
            }
            if (_logger.IsEnabled(LogLevel.Trace))
            {
                _logger.LogTrace($"File {fileInfo.FileName} was sent for download.");
            }

            return File(fileInfo.FileStream, fileInfo.ContentType); 
        }
    }
}
