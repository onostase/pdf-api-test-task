﻿using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using PDFStorage.Core.Contracts.QueryModels;
using PDFStorage.Core.Entities;
using PDFStorage.Persistence;
using PDFStorage.Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Tests.Repositories
{
    [TestFixture]
    public class FileRepositoryTests
    {
        private ApplicationDbContext _context;

        [SetUp]
        public void Setup()
        {
            var options = new DbContextOptionsBuilder<ApplicationDbContext>()
               .UseInMemoryDatabase(databaseName: "Add_writes_to_database")
               .Options;
            _context = new ApplicationDbContext(options);

            _context.Files.AddRange(new List<FileEntity> {
                new FileEntity { Id = 12, Name = "nameC", LocalFilePath = "path2"},
                new FileEntity { Id = 1, Name = "nameA", LocalFilePath = "path1"},
                new FileEntity { Id = 22, Name = "nameD", LocalFilePath = "path22"},
                new FileEntity { Id = 2, Name = "nameB", LocalFilePath = "path1.1"},
            });
            _context.SaveChanges();
        }

        [TearDown]
        public void TearDown()
        {
            _context.Database.EnsureDeleted();
            _context = null;
        }

        [Test]
        public async Task GetAllAsync_Query_CorrectSearch()
        {
            //Arrange
            var repository = new FileRepository(_context);
            var query = new Mock<IFileQueryModel>();
            query.SetupGet(x => x.Ids).Returns(new[] { 1,12,25});
            query.SetupGet(x => x.Names).Returns(new[] { "nameA", "nameC"});

            //Act
            var result = (await repository.GetAllAsync(query.Object).ConfigureAwait(false)).ToList();

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count);
            Assert.IsTrue(result.Any(x => x.Id == 1 && x.Name == "nameA"));
            Assert.IsTrue(result.Any(x => x.Id == 12 && x.Name == "nameC"));
        }

        [Test]
        public async Task GetAllAsync_SortQueryNameAsc_CorrectSort()
        {
            //Arrange
            var repository = new FileRepository(_context);
            var query = new Mock<IFileQueryModel>();
            query.SetupGet(x => x.OrderBy).Returns("Name");

            //Act
            var result = (await repository.GetAllAsync(query.Object).ConfigureAwait(false)).ToList();

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(4, result.Count);
            Assert.AreEqual("nameA", result.First().Name);
            Assert.AreEqual("nameD", result.Last().Name);
        }

        [Test]
        public async Task GetAllAsync_SortQueryNameDesc_CorrectSort()
        {
            //Arrange
            var repository = new FileRepository(_context);
            var query = new Mock<IFileQueryModel>();
            query.SetupGet(x => x.OrderBy).Returns("Name desc");

            //Act
            var result = (await repository.GetAllAsync(query.Object).ConfigureAwait(false)).ToList();

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(4, result.Count);
            Assert.AreEqual("nameD", result.First().Name);
            Assert.AreEqual("nameA", result.Last().Name);
        }

        [Test]
        public async Task GetAllAsync_SortQueryIdAsc_CorrectSort()
        {
            //Arrange
            var repository = new FileRepository(_context);
            var query = new Mock<IFileQueryModel>();
            query.SetupGet(x => x.OrderBy).Returns("Id");

            //Act
            var result = (await repository.GetAllAsync(query.Object).ConfigureAwait(false)).ToList();

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(4, result.Count);
            Assert.AreEqual(1, result.First().Id);
            Assert.AreEqual(22, result.Last().Id);
        }

        [Test]
        public async Task GetAllAsync_SortQueryIdDesc_CorrectSort()
        {
            //Arrange
            var repository = new FileRepository(_context);
            var query = new Mock<IFileQueryModel>();
            query.SetupGet(x => x.OrderBy).Returns("id desc");

            //Act
            var result = (await repository.GetAllAsync(query.Object).ConfigureAwait(false)).ToList();

            //Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(4, result.Count);
            Assert.AreEqual(22, result.First().Id);
            Assert.AreEqual(1, result.Last().Id);
        }

        [Test]
        public void GetAllAsync_SortQueryMoreThanOne_ThrowsException()
        {
            //Arrange
            var repository = new FileRepository(_context);
            var query = new Mock<IFileQueryModel>();
            query.SetupGet(x => x.OrderBy).Returns("id desc,name asc");

            //Act
            Func<Task> func = async () => await repository.GetAllAsync(query.Object).ConfigureAwait(false);

            //Assert
            Assert.That(func, Throws.ArgumentException);
        }

        [Test]
        public void GetAllAsync_SortQueryUnknown_ThrowsException()
        {
            //Arrange
            var repository = new FileRepository(_context);
            var query = new Mock<IFileQueryModel>();
            query.SetupGet(x => x.OrderBy).Returns("date desc");

            //Act
            Func<Task> func = async () => await repository.GetAllAsync(query.Object).ConfigureAwait(false);

            //Assert
            Assert.That(func, Throws.ArgumentException);
        }

        [Test]
        public async Task RemoveAsync_RemovesFromContext()
        {
            //Arrange
            var repository = new FileRepository(_context);
            var entity = _context.Files.Find(12);

            //Act
            var result = await repository.RemoveAsync(entity).ConfigureAwait(false);

            //Assert
            Assert.IsTrue(result);
            Assert.IsFalse(_context.Files.Any(x => x.Id == 12));
        }
    }
}
