﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using PDFStorage.Core.Contracts.Repositories;
using PDFStorage.Core.Contracts.Services;
using PDFStorage.Core.Entities;
using PDFStorage.Core.Exceptions;
using PDFStorage.Core.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Tests.Services
{
    [TestFixture]
    public class ManagementServiceTests
    {
        [Test]
        public async Task CreateFileRecordAsync_OkFile_DownloadsAndCreatesDbRecord()
        {
            //Arrange
            var repositoryMock = new Mock<IFileRepository>();
            repositoryMock.Setup(x => x.AddAsync(It.IsAny<FileEntity>(), It.IsAny<CancellationToken>())).ReturnsAsync(true);
            var uploadMock = new Mock<IUploadService>();
            uploadMock.Setup(x => x.DownloadFileToLocalFolderAsync(It.IsAny<Stream>(), It.IsAny<string>())).ReturnsAsync("path");
            var provider = GetServiceProvider(p => {
                p.AddTransient(a => repositoryMock.Object);
                p.AddTransient(a => uploadMock.Object);
            });
            var service = provider.GetService<ManagementService>();
            var streamMock = new Mock<Stream>();

            //Act
            await service.CreateFileRecordAsync("name", streamMock.Object).ConfigureAwait(false);

            //Assert
            uploadMock.Verify(x => x.DownloadFileToLocalFolderAsync(It.IsAny<Stream>(), It.IsAny<string>()), Times.Once);
            repositoryMock.Verify(x => x.AddAsync(It.Is<FileEntity>(f => f.Name == "name" && f.LocalFilePath =="path"), It.IsAny<CancellationToken>()), Times.Once);
        }

        [Test]
        public async Task CreateFileRecordAsync_OkFile_ReturnsViewModel()
        {
            //Arrange
            var repositoryMock = new Mock<IFileRepository>();
            repositoryMock.Setup(x => x.AddAsync(It.IsAny<FileEntity>(), It.IsAny<CancellationToken>()))
                .Callback<FileEntity, CancellationToken>((e, t) => e.Id = 1).ReturnsAsync(true);
            var provider = GetServiceProvider(p => {
                p.AddTransient(a => repositoryMock.Object);
            });
            var service = provider.GetService<ManagementService>();
            var streamMock = new Mock<Stream>();

            //Act
            var result = await service.CreateFileRecordAsync("name", streamMock.Object).ConfigureAwait(false);

            //Assert
            Assert.NotNull(result);
            Assert.AreEqual(1, result.Id);
            Assert.AreEqual("name", result.Name);
        }

        [Test]
        public void CreateFileRecordAsync_FailedFile_ThrowsException()
        {
            //Arrange
            var repositoryMock = new Mock<IFileRepository>();
            repositoryMock.Setup(x => x.AddAsync(It.IsAny<FileEntity>(), It.IsAny<CancellationToken>())).ReturnsAsync(true);
            var uploadMock = new Mock<IUploadService>();
            uploadMock.Setup(x => x.DownloadFileToLocalFolderAsync(It.IsAny<Stream>(), It.IsAny<string>())).ReturnsAsync(string.Empty);
            var provider = GetServiceProvider(p => {
                p.AddTransient(a => repositoryMock.Object);
                p.AddTransient(a => uploadMock.Object);
            });
            var service = provider.GetService<ManagementService>();
            var streamMock = new Mock<Stream>();

            //Act
            Func<Task> func = async () => await service.CreateFileRecordAsync("name", streamMock.Object).ConfigureAwait(false);

            //Assert
            Assert.That(func, Throws.Exception.TypeOf(typeof(FileProcessingFailedException)));
        }

        [Test]
        public async Task DeleteAsync_ExistingFile_DeletesSuccessfully()
        {
            //Arrange
            var repositoryMock = new Mock<IFileRepository>();
            repositoryMock.Setup(x => x.GetAsync(It.IsAny<int>(), It.IsAny<CancellationToken>())).ReturnsAsync(new FileEntity { Id = 1});
            repositoryMock.Setup(x => x.RemoveAsync(It.IsAny<FileEntity>(), It.IsAny<CancellationToken>())).ReturnsAsync(true);
            var provider = GetServiceProvider(p => {
                p.AddTransient(a => repositoryMock.Object);
            });
            var service = provider.GetService<ManagementService>();

            //Act
            var result = await service.DeleteAsync(1).ConfigureAwait(false);

            //Assert
            Assert.IsTrue(result);
        }

        [Test]
        public void DeleteAsync_NotFoundFile_ThrowsException()
        {
            //Arrange
            var repositoryMock = new Mock<IFileRepository>();
            repositoryMock.Setup(x => x.GetAsync(It.IsAny<int>(), It.IsAny<CancellationToken>())).ReturnsAsync((FileEntity)null);
            var provider = GetServiceProvider(p => {
                p.AddTransient(a => repositoryMock.Object);
            });
            var service = provider.GetService<ManagementService>();
            var streamMock = new Mock<Stream>();

            //Act
            Func<Task> func = async () => await service.DeleteAsync(12).ConfigureAwait(false);

            //Assert
            Assert.That(func, Throws.Exception.TypeOf(typeof(RecordNotFoundException)));
        }

        private IServiceProvider GetServiceProvider(Action<IServiceCollection> additionalRegistrations = null)
        {
            var services = new ServiceCollection();            

            var logger = new Mock<ILogger<ManagementService>>();
            services.AddTransient(p => logger.Object);
            var repositoryMock = new Mock<IFileRepository>();
            services.TryAddTransient(p => repositoryMock.Object);
            var uploadMock = new Mock<IUploadService>();
            uploadMock.Setup(x => x.DownloadFileToLocalFolderAsync(It.IsAny<Stream>(), It.IsAny<string>())).ReturnsAsync("path");
            services.TryAddTransient(p => uploadMock.Object);

            additionalRegistrations?.Invoke(services);

            services.TryAddTransient<ManagementService>();

            return services.BuildServiceProvider();
        }
    }
}
