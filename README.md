# pdf-api-test-task

Test task implementation

How to use API:
* To upload a file, use endpoint api/files and make a POST request with a form parameter named 'file' of type 'File' to it.
* To See a list of files, make GET request to api/files endpoint
* To sort files by Id or Name make GET request to an endpoint api/files and pass query parameter _orderby_, e.g.: ```orderby=name desc``` . Only one sort parameter is supported.
* To see specific file information, make GET request to api/files/{id}
* To delete specific file record, make DELETE request to api/files/{id}
* To download file, use endpoint api/download{id} with GET request

**_Note_**: Local Mssql database is used.